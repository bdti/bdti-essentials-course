![banner](img/Session1.png)
# User guide - Session 1:

[Watch the recording](https://www.youtube.com/watch?v=J1DblYCynOM&list=PLyMUk47rPuqowFD0-4lQuQ-XiffMI5Av6&index=1)

## Data Access and Exploration

This document is a step-by-step guide for you to replicate what we have already done in session 1.

### Goal of Session 1
    
Getting familiar with the Big Data Test Infrastructure. Lay the foundation for data analysis by loading and exploring the relevant datasets.
    
### Relevant Tools (and links to download them locally) 
    

1. KNIME Analytic Platform
https://www.knime.com/downloads


2. R-studio
https://rstudio-education.github.io/hopr/starting.html

3. Jupyter Notebooks
https://jupyter.org/install



Plese refer to the [Learning Resources section](/README.md) where you can find a list of open and free online resources to learn more about using Python and R for data analysis. 

## Step by step guide

Exercise: 

You will find this first session exercise done with KNIME and the instructions in the [“Session_1.knwf”](https://code.europa.eu/bdti/bdti-essentials-course/-/blob/master/Session%201:%20Data%20Access%20and%20Exploration/Exercise_S1.knwf) file. The solutions are under "Solution_S1.knwf".


    
Each subsection covers a step of the approach, namely:

- Step 1.1: Initialize the KNIME instance
- Step 1.2: Initialize R-studio instance
- Step 1.3: Initialize Jupyter Notebook instance

- Step 2.1: Inside the KNIME instance, download the datasets
- Step 2.2: Open KNIME and create a new workflow
    - Read the different data files and view their content.
- Step 2.3: Open R-studio and read the data
- Step 2.4: Open Jupyter Notebook and read the data with Python

---

### Step 1.1: Initialize KNIME

1. Go on the Service Catalog section of the Portal.
2. Find the KNIME badge and click 'Launch'.
3. Assign a name to the instance and select a group from the ones available.
4. Select the sharing status and the default configuration.
5. Copy the auto-generated password. This will be needed to access the instance later.
6. Select the NFS PVC name corresponding to the DSL group selected at point 3.
7. Go to the My Services tab and wait for the Knime status to turn active after a few minutes.

### Step 1.2: Initialize R-studio instance

1. Go on the Service Catalog section of the Portal.
2. Find the R-studio badge and click 'Launch'.
3. Assign a name to the instance and select a group from the ones available.
4. Select the sharing status and the default configuration.
5. Copy the auto-generated password. This will be needed to access the instance later.
6. Select the NFS PVC name corresponding to the DSL group selected at point 3.
7. Go to the My Services tab and wait for the Knime status to turn active after a few minutes.

### Step 1.3: Initialize Jupyter Notebook instance

1. Go on the Service Catalog section of the Portal.
2. Find the Jupyterlab - lab-4.0.4 - datascience-notebook badge and click 'Launch'.
3. Assign a name to the instance and select a group from the ones available.
4. Select the sharing status and the default configuration.
5. Copy the auto-generated password. This will be needed to access the instance later.
6. Select the NFS PVC name corresponding to the DSL group selected at point 3.
7. Go to the My Services tab and wait for the Knime status to turn active after a few minutes.

### Step 2.1: Inside the Knime instance, download, unzip, and place the data in the correct storage folder.

#### Download
1. Go to My Services on the portal and open the Knime instance.
2. Log in using the password created in step 1. (you can always retrieve passwords from the My Data tab).
3. Once you logged in, you see that you have entered a Virtual Machine with a desktop containing FireFox and Knime.
4. Open Firefox, select 'execute' in the pop-up, and go to the corresponding websites and download the data indicated in the [Datasets section](/README.md) in the main page

#### Unzip
1. Open the Downloads folder and double-click the “CORDIS”
2. You need to select which program you want to open the folder. Choose applications --> xArchiver.
3. Select 'Extract files'. Change the destination folder to the Downloads folder by clicking the little icon in the selection bar. Extract the files.
4. The other 2 datasets are in the correct format and can be moved without manipulation to your efs-storage folder
<img width="246" alt="image" src="img/setupData.png">

### Step 2.2: Open KNIME and create a new workflow
-	Read the different data files and view their content.

1. Go back to the desktop view open Knime, and select 'execute' in the pop-up.
2. In the next pop-up select the default workspace '/root/knime-workspace'/efs-storage click open, then click ‘launch’.
3. You are now running KNIME. 
4. On the left bar, go to ‘space explorer’ --> Create a Workflow


<img width="246" alt="image" src="img/session1_2.png">


5. Go to the Node Repository and start building your workflow
  - Select “CSV Reader” --> open configuration --> click ‘browse’ and go to the folder where your data is stored -> select csv file, define column delimiter (; in this case), and click ‘Ok’
  - The light of your node will turn yellow, click run to read your data.
<img width="241" alt="image" src="img/session1_3.png">

  - Keep the process with the other data files (you can find the instructions in the “Session_1.knwf” file.
6. Don’t forget to always save!


### Step 2.3: Open R-studio and read the data

1.	Go back to My Services and open your R-studio instance.
2.	User: “rstudio”, password: the one you saved in 1.2
3.	Access the data with the “console”. The following script is the one we used in this session.

```
install.packages("jsonlite")

library(jsonlite)

#Specify the path to JSON file
json_file_path <- "/home/rstudio/H2020/json/project.json"

#Read the JSON file
project_data <- fromJSON(json_file_path)

#Flatten data to tabular format
project_h2020 <- jsonlite::flatten(project_data)

#View of the data
View(project_h2020)}
```

### Step 2.4: Open Jupyter Notebook and read the data with Python

1.	Go back to My Services and open your Jupyter Notebook instance.
2.	Password: the one you saved in 1.3
3.	A ‘launcher’ page will pop up, choose “Python 3”.
4.	Access the CO2 data by following the script used in this first session.

```
# import the libraries we need to work with
import pandas as pd

# read the data
co2_data = pd.read_csv('owid-co2-data.csv')

# check the first 10 rows and explore the columns and data types
co2_data.head(10)

# find unique values in a column
co2_data.country.unique()

# count occurrences of the countries
value_counts = co2_data['country'].value_counts()

#  let's find a specific country and it's count
# 0 is the default value returned if the country is not found
value_counts.get('Switzerland', 0) 

```


### Have fun!






