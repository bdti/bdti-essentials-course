![banner](img/Session4.png)
# User guide - Session 4:

[Watch the recording](https://www.youtube.com/watch?v=zZQ2kAjNeqY)

## Data Analysis

This document is a step-by-step guide for you to replicate what we have already done in session 4.

### Goal of session 4:
Begin the analytical process by addressing the core objectives.
Zoi has the data ready to work, now she wants to build a dashboard with some visualisations and generate a pdf report from it.

### Relevant tools (and links to download them locally)
- KNIME Analytics Platform https://www.knime.com/downloads
- PgAdmin https://www.pgadmin.org/download/
- PostgreSQL https://www.postgresql.org/download/
- Apache Superset https://superset.apache.org/
  
This document is meant to guide the user through session 4 - **Analytics**

The guide will be a step by step tutorial towards such objective. More in detail, each subsection covers a step of the approach, namely:
  
  Step 1: Initialize the resources.
  
  Step 2: Connect PgAdmin to PostgreSQL

  Step 3: Configure Apache Superset and connect the PostgreSQL database
  
  Step 4: Connect the PostgreSQL database to KNIME 

  Step 5: Apply Analytics

## Step 1: Initialize the resources
As first step, the user should inizialize the required resources. More in particular, three instances should be launched:

- PostgreSQL instance
- PgAdmin instance
- KNIME instance
  
### Initialize the PostgreSQL instance
1. Go on the Service Catalog section of the Portal.
2. Click on the button Launch on the PostgreSQL badge.
3. Assign a name to the instance and select a group from the ones available in the list.
4. Select the micro configuration.
5. Copy the auto-generated password. This will be needed to access the instance in the later stage. (NB: Instance credentials are automatically saved and acessible on the My Data section of the portal).
6. Once the instance is deployed, it will be possible to copy its host address by clicking on the related button Copy in the My Services section of the portal.

### Initialize the PgAdmin instance
1. Go on the Service Catalog section of the Portal.
2. Click on the button Launch on the PgAdmin4 badge.
3. Assign a name to the instance and select a group from the ones available in the list.
4. Select the default configuration.
5. Set your PgAdmin email and copy the auto-generated password. This will be needed to access the instance in the later stage. (NB: Instance credentials are automatically saved and accessible on the My Data section of the portal)..
6. Launch the instance by clicking on the launch button.

### Initialize the KNIME instance
1. Go on the Service Catalog section of the Portal.
2. Click on the button Launch on the Knime badge.
3. Assign a name to the instance and select a group from the ones available in the list.
4. Select the default configuration.
5. Copy the auto-generated password. This will be needed to access the instance in the later stage. (NB: Instance credentials are automatically saved and acessible on the My Data section of the portal).
6. Select the NFS PVC name corresponding to the DSL group selected at point 3.
7. Launch the instance by clicking on the launch button.

### Initialize the Apache Superset instance
1. Go on the Service Catalog section of the Portal.
2. Click on the button Launch on the Apache Superset badge.
3. Assign a name to the instance and select a group from the ones available in the list.
4. Select the micro configuration.
5. Set your Admin username, Admin email, Admin firstname and Admin lastname. Copy the auto-generated password. This will be needed to access the instance in the later stage. (NB: Instance credentials are automatically saved and accessible on the My Data section of the portal).
6. Launch the instance by clicking on the launch button.

After having launch the instances, go on the My Services section of the portal to verify that all the deployed services are up and running (all three instances should be in status ACTIVE).

## Step 2: Configure pgAdmin and connect the PostgreSQL instance
1. From the My Services section of the portal, click on the Copy button of the PostgreSQL instance to copy the host address of the instance in the clipboard.
2. The PostgreSQL host address is in the format:

        <postgreSQLHost>:5432
   
4. Go on the My Services section of the portal and open the pgAdmin instance.

5. Login into your pgAdmin account whith the credentials defined in the configuration.
6. In the main page, click on the Browser tab: this will allow you to browse on all available resources on your pgAdmin account.
7. On the Browser tab, right click on the Servers item > Register > Servers... . 
This will open up the configuration modal for registering a new server on our pgAdmin account: the PostgreSQL database will be added.

8. The configuration modal contains five fields that need to be compiled:
      - *Hostname/address*: this is the hostname/address of the PostgreSQL instance. Input here the *postgresSQLHost* (see Step 2.2)
      - *Port*: 5432
      - *Maintenance Database*: postgres
      - *Username*: postgres
      - *Password*: input the PostgreSQL password defined in the configuration.

## Step 3: Configure Apache Superset and connect the PostgreSQL database

### Add the database to Apache Superset
1. Go on the My Services section of the portal and open the Apache Superset instance.
2. Login into your Apache Superset instance whith the credentials defined in the configuration.
3. In the navbar, click on Data > Databases.
4. Click on the " + DATABASE " button. This will open the Connect a database tool that will allow us to connect the PostgreSQL database to Apache Superset.
5. Select PostgreSQL as database to connect.
6. Enter the required PostgreSQL credentials:
  - HOST: this is the hostname/address of the PostgreSQL instance. Input here the <postgreSQLHost> (see Step 2.2)
  - PORT: 5432
  - DATABASE NAME: postgres
  - USERNAME: postgres
  - PASSWORD: input the autogenerated PostgreSQL password. You can retrieve it from the My Data section of the portal.
7. Click on Connect.

### Add the dataset to Apache Superset
1. In the navbar, click on Data > Datasets.
2. Click on the " + DATASETS " button. This will open the Add dataset tool that will allow us to connect the dataset from the PostgreSQL database to Apache Superset.
3. In the DATABASE field, select the database that has been connected to Apache Superset in the previous step.
4. In the SCHEMA field, select public.
5. In the SEE TABLE SCHEMA field, select the table that contains the data imported in the previous steps (i.e. energy_balance_eu).
6. If the dataset has been connected succesfully, it should be visible in the list of available datasets.
7. For the newly connected dataset, in the Actions section press on the Edit icon.
8. Go on COLUMNS and tick the property Is temporal and Default datetime for the column time_period. This will allow Apache Superset to deal with such column as a temporal dimension, and this will be needed for all time-related data visualizations (eg. bar charts, time series, ...).
9. Save the changes by clicking on the Save button.


## Step 4: Connect the PostgreSQL database to KNIME 
Follow the instructions in the exercise to connect KNIME to your database ("Exercise_S4.knwf")

In your KNIME instance add the following nodes after the last join operation:
- “PostgreSQL Connector”: open the configuration and add your database credentials
- "DB Table Selector": select the tables to work with
- "DB Reader": this node automatically reads the tables selected

## Step 5: Apply Analytics
Follow the instructions in the exercise to apply the analytic techniques we learnt during the session using KNIME ("Exercise_S4.knwf")

You can refer to the slides to follow all these steps.

## Have fun!


