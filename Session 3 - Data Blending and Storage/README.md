![banner](img/session3.png)
# User guide - Session 3

## Data Blending and Storage

[Watch the recording](https://youtu.be/DcmR2v0PGr0?si=BrIu2tJIjEsGvBvd)


### Goal of session 3:

In this session, we will continue our journey through the Big Data Test Infrastructure (BDTI), by learning techniques to blend data together

### Relevant tools (and links to download them locally)

- KNIME Analytics Platform https://www.knime.com/downloads
- PgAdmin https://www.pgadmin.org/download/
- PostgreSQL https://www.postgresql.org/download/

- SQLite https://www.sqlite.org/ 


This document is meant to guide the user through session 3 - **Data Blending and Storage**

After the data is clean, Zoi will learn how to blend all the different data sources into a final table to keep working with only one centralized file, finalizing the ETL process and storing the data in different formats such as CSV and in a database

The guide will be a step by step tutorial towards such objective. More in detail, each subsection covers a step of the approach, namely:
  
  Step 1: Initialize the resources.
  
  Step 2: Connect PgAdmin to PostgreSQL
  
  Step 3: Perform Data Blending and Storage
  
  Step 4: Connect the database to KNIME and push the data

## Step 1: Initialize the resources

As first step, the user should inizialize the required resources. More in particular, three instances should be launched:

- PostgreSQL instance
- PgAdmin instance
- KNIME instance

Please check our Resource section in the main page of the repository.
  
### Initialize the PostgreSQL instance

1. Go on the Service Catalog section of the Portal.
2. Click on the button Launch on the PostgreSQL badge.
3. Assign a name to the instance and select a group from the ones available in the list.
4. Select the micro configuration.
5. Copy the auto-generated password. This will be needed to access the instance in the later stage. (NB: Instance credentials are automatically saved and acessible on the My Data section of the portal).
6. Once the instance is deployed, it will be possible to copy its host address by clicking on the related button Copy in the My Services section of the portal.

### Initialize the PgAdmin instance

1. Go on the Service Catalog section of the Portal.
2. Click on the button Launch on the PgAdmin4 badge.
3. Assign a name to the instance and select a group from the ones available in the list.
4. Select the default configuration.
5. Set your PgAdmin email and copy the auto-generated password. This will be needed to access the instance in the later stage. (NB: Instance credentials are automatically saved and accessible on the My Data section of the portal)..
6. Launch the instance by clicking on the launch button.

### Initialize the KNIME instance

1. Go on the Service Catalog section of the Portal.
2. Click on the button Launch on the Knime badge.
3. Assign a name to the instance and select a group from the ones available in the list.
4. Select the default configuration.
5. Copy the auto-generated password. This will be needed to access the instance in the later stage. (NB: Instance credentials are automatically saved and acessible on the My Data section of the portal).
6. Select the NFS PVC name corresponding to the DSL group selected at point 3.
7. Launch the instance by clicking on the launch button.
   
After having launch the instances, go on the My Services section of the portal to verify that all the deployed services are up and running (all three instances should be in status ACTIVE).

## Step 2: Configure pgAdmin and connect the PostgreSQL instance
1. From the My Services section of the portal, click on the Copy button of the PostgreSQL instance to copy the host address of the instance in the clipboard.
2. The PostgreSQL host address is in the format:

        <postgreSQLHost>:5432
   
4. Go on the My Services section of the portal and open the pgAdmin instance.

5. Login into your pgAdmin account whith the credentials defined in the configuration.
6. In the main page, click on the Browser tab: this will allow you to browse on all available resources on your pgAdmin account.
7. On the Browser tab, right click on the Servers item > Register > Servers... . 
This will open up the configuration modal for registering a new server on our pgAdmin account: the PostgreSQL database will be added.

8. The configuration modal contains five fields that need to be compiled:
      - *Hostname/address*: this is the hostname/address of the PostgreSQL instance. Input here the *postgresSQLHost* (see Step 2.2)
      - *Port*: 5432
      - *Maintenance Database*: postgres
      - *Username*: postgres
      - *Password*: input the PostgreSQL password defined in the configuration.

## Step 3: Perform Data Blending and Storage

Follow the instructions in the exercise and review the slides to replicate what we did during the session!
- "Exercise_S3.knwf"

## Step 4: Connect the database to KNIME and push the data

Follow the instructions in the exercise to connect KNIME to your database ("Exercise_S3.knwf")

In your KNIME instance add the following nodes after the last join operation:
- “PostgreSQL Connector”: open the configuration and add your database credentials
- "DB Writer": give a name to your data and choose which columns to include
  
You can refer to the slides to follow all these steps.


## Have fun!
