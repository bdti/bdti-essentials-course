![banner](img/Session5.png)

# User guide - Session 5

[Watch the recording - available soon]()

## Advanced Module: Gathering Data from the Web and Geo Visualisations

### Goal of session 5:
Enhance data analysis capabilities by connecting to external data sources.  

Learn to extract data from the web with ease by learning the concept of an API.


Retrieve geocode of universities of interest by using their addresses and visualize them in a map.​Learn how to dynamize this process

### ***External resources***
In this session we are using the power of APIs, remember to always read the documentation and follow their specific instructions.
Zoi and team will connect to the API of Open Street Map.

[OpenStreetMap](https://www.openstreetmap.org/#map=7/50.510/4.475) provides map data for thousands of websites, mobile apps, and hardware devices
OpenStreetMap is built by a community of mappers that contribute and maintain data about roads, trails, cafés, railway stations, and much more, all over the world.

You can follow the specific documentation we are going to use here:

https://nominatim.org/release-docs/develop/api/Search/

---


### Relevant tools (and links to download them locally)
- KNIME Analytics Platform https://www.knime.com/downloads
- PgAdmin https://www.pgadmin.org/download/
- PostgreSQL https://www.postgresql.org/download/
  
This document is meant to guide the user through session 5 - **Advanced Module**

The guide will be a step by step tutorial towards such objective. More in detail, each subsection covers a step of the approach, namely:
  
  Step 1: Initialize the resources.
  
  Step 2: Connect PgAdmin to PostgreSQL
  
  Step 3: Connect the PostgreSQL database to KNIME 
  
  Step 4: Connect to the OSM API and request Geo data

## Step 1: Initialize the resources

As first step, the user should inizialize the required resources. More in particular, three instances should be launched:

- PostgreSQL instance
- PgAdmin instance
- KNIME instance
  
### Initialize the PostgreSQL instance
1. Go on the Service Catalog section of the Portal.
2. Click on the button Launch on the PostgreSQL badge.
3. Assign a name to the instance and select a group from the ones available in the list.
4. Select the micro configuration.
5. Copy the auto-generated password. This will be needed to access the instance in the later stage. (NB: Instance credentials are automatically saved and acessible on the My Data section of the portal).
6. Once the instance is deployed, it will be possible to copy its host address by clicking on the related button Copy in the My Services section of the portal.

### Initialize the PgAdmin instance
1. Go on the Service Catalog section of the Portal.
2. Click on the button Launch on the PgAdmin4 badge.
3. Assign a name to the instance and select a group from the ones available in the list.
4. Select the default configuration.
5. Set your PgAdmin email and copy the auto-generated password. This will be needed to access the instance in the later stage. (NB: Instance credentials are automatically saved and accessible on the My Data section of the portal)..
6. Launch the instance by clicking on the launch button.

### Initialize the KNIME instance
1. Go on the Service Catalog section of the Portal.
2. Click on the button Launch on the Knime badge.
3. Assign a name to the instance and select a group from the ones available in the list.
4. Select the default configuration.
5. Copy the auto-generated password. This will be needed to access the instance in the later stage. (NB: Instance credentials are automatically saved and acessible on the My Data section of the portal).
6. Select the NFS PVC name corresponding to the DSL group selected at point 3.
7. Launch the instance by clicking on the launch button.

## Step 2: Configure pgAdmin and connect the PostgreSQL instance

1. From the My Services section of the portal, click on the Copy button of the PostgreSQL instance to copy the host address of the instance in the clipboard.
2. The PostgreSQL host address is in the format:

        <postgreSQLHost>:5432
   
4. Go on the My Services section of the portal and open the pgAdmin instance.

5. Login into your pgAdmin account whith the credentials defined in the configuration.
6. In the main page, click on the Browser tab: this will allow you to browse on all available resources on your pgAdmin account.
7. On the Browser tab, right click on the Servers item > Register > Servers... . 
This will open up the configuration modal for registering a new server on our pgAdmin account: the PostgreSQL database will be added.

8. The configuration modal contains five fields that need to be compiled:
      - *Hostname/address*: this is the hostname/address of the PostgreSQL instance. Input here the *postgresSQLHost* (see Step 2.2)
      - *Port*: 5432
      - *Maintenance Database*: postgres
      - *Username*: postgres
      - *Password*: input the PostgreSQL password defined in the configuration.

## Step 3: Connect the PostgreSQL database to KNIME 
Follow the instructions in the exercise to connect KNIME to your database ("Exercise_S4.knwf")

In your KNIME instance add the following nodes after the last join operation:
- “PostgreSQL Connector”: open the configuration and add your database credentials
- "DB Table Selector": select the tables to work with
- "DB Reader": this node automatically reads the tables selected

## Step 4: Connect to the OSM API and request Geo data
Follow the instructions in the exercise to apply the analytic techniques we learnt during the session using KNIME ("Exercise_S5.knwf")

You can refer to the slides to follow all these steps.

## Have fun!
