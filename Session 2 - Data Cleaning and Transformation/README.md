![banner](img/Session2.png)
# User guide - Session 2

## Data Cleaning and Transformation

[Watch the recording](https://www.youtube.com/watch?v=-UAFZmAxd70&list=PLyMUk47rPuqowFD0-4lQuQ-XiffMI5Av6&index=2)


### Goal of session 2:

In this session, we will continue our journey through the Big Data Test Infrastructure (BDTI), focusing on preparing your data for meaningful analysis. Our primary goal is to equip you with the skills needed to clean, transform, and ensure your datasets are in the right format for subsequent analysis. 

### Relevant tools (and links to download them locally)

In this session, we are going to show how to use clean and transform our opendata using KNIME; 

- KNIME Analytics Platform https://www.knime.com/downloads

The same tasks and operations can be easily done using Jupyter Notebook and R-Studio but this requires a bit more experience in code development. Plese refer to the [Learning Resources section](/README.md) where you can find a list of open and free online resources to learn more about using Python and R for data analysis. 

## Step by step guide

Exercise:

You will find this first session exercise done with KNIME and the instructions to follow in the [Exercise_S2.knwf](https://code.europa.eu/bdti/bdti-essentials-course/-/blob/master/Session%202:%20Data%20Cleaning%20and%20Transformation/Exercise_S2.knwf) file. 


### Have fun!
