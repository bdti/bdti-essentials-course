Copyright 2023 European Union

Documentation in this repository is licensed under the Creative Commons Attribution 4.0 License, and code samples are licensed under the BSD 3-Clause licence.

![Alt text](img/BDTI_Banner_generic.png)


# Big Data Test Infrastructure (BDTI) Essentials Course
## Enabling a Data-informed Public Sector: An Introductory Course to BDTI Essentials
 
The [Big Data Test Infrastructure](https://big-data-test-infrastructure.ec.europa.eu/index_en) (BDTI)
is ready-to-use, free of charge, analytics cloud stack for the public sector offered to all European public administrations to experiment with open-source tools and foster the re-use of public sector data for a data-informed public sector.

In this repo, you can find the code and material developed session by session, during the BDTI Essential course published in our [webiste](https://big-data-test-infrastructure.ec.europa.eu/resources/courses-and-training/bdti-essentials-course_en). 

This free course aims to help public administrations explore the free-of-charge data analytics playground the Big Data Test Infrastructure (BDTI) offers through a practical use case. After this course, you will be ready to apply for BDTI and build a public sector data use case using the platform and open source data analytics [tools](https://big-data-test-infrastructure.ec.europa.eu/service-offering_en).

This course will show you how to harness open data sources to address a real-world application by leveraging the resources offered by data.europa.eu. During the course, you will learn how to analyse funding allocated for research and innovation to universities across EU nations with high carbon emissions. Through this use case, you'll be able to localise and better understand green initiatives and how future partnerships can be developed.

This course is taught in English and is for people (public servants, professionals, and students) supporting the public sector in exploring how to derive insights from data. In terms of the level of knowledge required for this course, this series of courses is accessible to all levels as it starts with foundational courses. Each session can be replicated both in the BDTI playground for the public administrations applying to the program and standalone on your own notebook following the instructions.

Crafted for a diverse audience, the course offers an in-depth exploration of the Big Data Test Infrastructure (BDTI) cloud stack, empowering participants with the knowledge to leverage open-source tools for data analytics. The curriculum is tailored to unfold over five sessions, each carefully designed to enhance understanding of data access, processing, and visualisation, fostering a practical approach to data analytics.

In this Gitlab repository, you will find all the files to replicate the different sessions. Each folder contains a "user guide" with instructions for you to follow.

Subscribe to our [BDTI Essential YouTube Playlist](https://youtube.com/playlist?list=PLyMUk47rPuqowFD0-4lQuQ-XiffMI5Av6&si=YwLBEe2KEjGsBq2K)


Interested in playing with BDTI? For any question or request for support, please visit our [webiste](https://big-data-test-infrastructure.ec.europa.eu/apply-bdti_en) and contact us via EC-BDTI-PILOTS@ec.europa.eu


| Session | Date | Description | Components | Slides and Exercises | Recording |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| [Session 1: Data Access and Exploration](https://code.europa.eu/bdti/bdti-essentials-course/-/tree/master/Session%201:%20Data%20Access%20and%20Exploration) | 02/02/24 11:00 AM - 12:15 PM CEST | Lay the foundation for data analysis by loading and exploring the relevant datasets. In this first session, we'll present the use case, introducing BDTI and one of our data analysis tools.You'll learn to download, load and access data in different formats. You'll begin your first exploration of the data and decide what data are useful to complete this task during the following sessions. | KNIME Analytic Platform, R-studio, Jupyter Notebooks |[Session 1 - Slide and exercises](https://code.europa.eu/bdti/bdti-essentials-course/-/tree/master/Session%201:%20Data%20Access%20and%20Exploration) | [Session 1 video](https://www.youtube.com/watch?v=J1DblYCynOM&list=PLyMUk47rPuqowFD0-4lQuQ-XiffMI5Av6&index=1)|
| [Session 2: Data Cleaning and Transformation](https://code.europa.eu/bdti/bdti-essentials-course/-/tree/master/Session%202%3A%20Data%20Cleaning%20and%20Transformation) | 16/02/24 11:00 AM - 12:15 PM CEST |Prepare the data for analysis by cleaning and transforming it. This week, you'll clean your data by addressing data quality issues like missing values. You'll learn specific techniques for data cleaning and transformation.​ And you'll prepare the datasets for further analysis.|KNIME Analytic Platform|[Session 2 - Slide and exercises](https://code.europa.eu/bdti/bdti-essentials-course/-/tree/master/Session%202%3A%20Data%20Cleaning%20and%20Transformation)|[Session 2 video](https://www.youtube.com/watch?v=-UAFZmAxd70&list=PLyMUk47rPuqowFD0-4lQuQ-XiffMI5Av6&index=2)|
| [Session 3: Data Blending and Storage](https://code.europa.eu/bdti/bdti-essentials-course/-/tree/master/Session%203%20-%20Data%20Blending%20and%20Storage) | 01/03/24 11:00 AM - 12:15 PM CEST | Learn techniques for automating data blending and storage.This week, you’ll identify when multiple data files have the same structure (columns) and can be concatenated.| PostgreSQL, PgAdmin, KNIME Analytic Platform |[Session 3 - Slide and exercises](https://code.europa.eu/bdti/bdti-essentials-course/-/tree/master/Session%203%20-%20Data%20Blending%20and%20Storage)|[Session 3 video](https://youtu.be/DcmR2v0PGr0?si=BrIu2tJIjEsGvBvd)|
| [Session 4: Basic Analytics](https://code.europa.eu/bdti/bdti-essentials-course/-/tree/master/Session%204%20-%20Data%20analytics?ref_type=heads) | 15/03/24 11:00 AM - 12:15 PM CEST | Begin the analytical process by addressing the core objectives.​This week, you’ll be learning how to get insights from the data and build a report.|PostgreSQL, PgAdmin, KNIME Analytic Platform, Apache Superset|[Session 4 - Slide and exercises](https://code.europa.eu/bdti/bdti-essentials-course/-/tree/master/Session%204%20-%20Data%20analytics?ref_type=heads)|[Session 4 video](https://www.youtube.com/watch?v=zZQ2kAjNeqY)
| [Session 5: Advanced Module: Gathering Data from the Web and Geo Visualisation​](https://code.europa.eu/bdti/bdti-essentials-course/-/tree/master/Session%205%20-%20Data%20gathering?ref_type=heads) | 22/03/24 11:00 AM - 12:15 PM CEST | Enhance data analysis capabilities by connecting to external data sources. You'll learn how to tell your data's story gathering additional geo data using APIs from the web and build more advanced data visualisation techniques using data visualisation tools from BDTI.|KNIME Analytic Platform, Apache Superset, Open Street Map APIs|[Session 5 - Slide and exercises](https://code.europa.eu/bdti/bdti-essentials-course/-/tree/master/Session%205%20-%20Data%20gathering?ref_type=heads)|[Session 5 video](https://www.youtube.com/watch?v=ZcEkEtcnzlo)

### Use case

![Alt text](img/usecase.PNG)

### Datasets (open data)

Data analysis is a lot like cooking. You need specific and good ingredients/data, know where to find them, and have the right tools to work with.

In this training we use open data. Open data is data that anyone can access, use and share. Governments, businesses and individuals can use open data to bring about social, economic and environmental benefits.

Discover [data.europe.eu](https://data.europa.eu/) the single point of access to open data from European countries, EU institutions, agencies and bodies and other European countries.

#### 1) Horizon projects from data.europa.eu


These datasets contain  information about projects and their results funded by the European Union under the Horizon 2020 framework programme 

**Horizon 2014 - 2020**


Horizon Data: this data comes in a compressed format (zip), therefore step 2.2 will help you to unzip the files inside the VM.


https://data.europa.eu/data/datasets/cordish2020projects?locale=en

File Names 
- euroSciVoc_2020.xlsx (excel)
- organization_2020.csv (csv)
- project_2020.json (json)

**Horizon 2021 - 2027**

https://data.europa.eu/data/datasets/cordis-eu-research-projects-under-horizon-europe-2021-2027?locale=en

File Names
- euroSciVoc_2021.xlsx (excel)
- organization_2021.csv (csv)
- project_2021.json (JSON)

#### 2) Official European Union Country Names from Eurostat

[Eurostat](https://ec.europa.eu/eurostat)

*This file will automatically download by clicking the url.*

https://ec.europa.eu/eurostat/statistics-explained/images/9/9f/Country_Codes_and_Names.xlsx

File Name
- Country_Codes_and_Names.xlsx

#### 3) CO2 emissions from Our World in Data

[Our World in Data](https://ourworldindata.org/)

https://ourworldindata.org/co2-emissions

https://github.com/owid/co2-data

File Name
- owid-co2-data.csv (csv)

#### 4) Open Street Map API - Nominatim

Nominatim is a search engine for OpenStreetMap data. You may search for a name or address (forward search) or look up data by its geographic coordinate (reverse search).


https://nominatim.openstreetmap.org/ui/search.html 

---
### Learning Resources

In this section you can find some open and online learning resources related to the tools we are going to use in the different sessions

#### Jupyter Notebooks
- [Phyton Data science Handbook](https://jakevdp.github.io/PythonDataScienceHandbook/) 


#### R Studio

- [R for Data Science](https://r4ds.had.co.nz/index.html)

#### KNIME

- [Extensive Resources for Learning KNIME](https://www.knime.com/resources)

- [Online self-paced courses](https://www.knime.com/learning)

- [Explore KNIME Hub for examples](https://hub.knime.com/?pk_vid=7d34f4355d5280d617077541228684a7)

- [Documentation to keep your team up to speed with best practices for data science](https://docs.knime.com/?pk_vid=7d34f4355d5280d617077541638684a7)

- [KNIME free books](https://www.knime.com/knimepress)

- [Ask for help on KNIME Forum](https://forum.knime.com/)

- [Videos on KNIMETV](https://www.youtube.com/user/KNIMETV)

#### SQL


- [Databases: Relational Databases and SQL](https://online.stanford.edu/courses/soe-ydatabases0005-databases-relational-databases-and-sql), free training from Stanford on Edx.

- [O'Reilly "SQL Fundamentals for Data" online training](https://github.com/thomasnield/oreilly_sql_fundamentals_for_data) 

- [Learn SQL Basics for Data Science Specialization](https://www.coursera.org/specializations/learn-sql-basics-data-science), free training from UCDavis University in Coursera.


#### Statistical learning

- [An Introduction to Statistical learning in Python and R](https://www.statlearning.com/)



#### 
- [Eurostat Statistics Explained articles](https://github.com/eurostat/statistics-coded): Usefull resources for reproducing some of the visualisations in Eurostat Statistics Explained articles and getting familiar with Phyton, R, SQL and other topics presented in the training


---


The code in this repository has been developed during the BDTI Essential Course and is intended for training purposes only. The code is provided "as is" without warranty of any kind, either express or implied, including, but not limited to, any implied warranty against infringement of third parties' property rights, of merchantability, integration, satisfactory quality and fitness for a particular purpose.

Interested in playing with BDTI? For any question or request for support, please visit our [webiste](https://big-data-test-infrastructure.ec.europa.eu/apply-bdti_en) and contact us via EC-BDTI-PILOTS@ec.europa.eu
